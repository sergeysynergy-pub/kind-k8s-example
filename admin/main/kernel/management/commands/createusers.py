from django.core.management.base import BaseCommand
#
import app.settings as settings
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Creating users from settings.USERS_TO_CREATE'

    def handle(self, *args, **options):
        print("Creating users...")
        if hasattr(settings, 'USERS_TO_CREATE'):
            for user in settings.USERS_TO_CREATE:
                try:
                    # Check user exists
                    User.objects.get(username=user['username'])
                    self.stdout.write(self.style.WARNING(
                        'User "%s" already exists' % user['username']
                    ))
                except User.DoesNotExist:
                    # Create new user
                    try:
                        newUser = User.objects.create_user(
                            user['username'],
                            '',
                            user['password'],
                        )
                        newUser.is_superuser = user['is_superuser']
                        newUser.is_staff = user['is_staff']
                        newUser.save()
                        self.stdout.write(self.style.SUCCESS(
                            'User has been created: %s' % user['username']
                        ))
                    except Exception as e:
                        self.stdout.write(self.style.ERROR(
                            'Failed to create user %s' % (user['username'], e)
                        ))
        else:
            print("Dictionayr USERS_TO_CREATE not found")
