#!/bin/sh

if [[ $1 = "init" ]]; then
  python manage.py makemigrations
  python manage.py migrate
  python manage.py createusers
  python manage.py collectstatic --noinput
fi


if [[ $1 = "gunicorn" ]]; then
  echo "Start web-server gunicorn"
  pkill -f runserver
  gunicorn --reload -w 2 -c ./app/gunicorn.conf.py app.wsgi &
  exit 0
fi

if [[ $1 = "dj" ]]; then
  pkill gunicorn
  pkill -f runserver
  python manage.py runserver 0.0.0.0:8000
  exit 0
fi


if [[ $1 = "djstop" ]]; then
  pkill -f runserver
  exit 0
fi


