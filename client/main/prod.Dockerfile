FROM nginx

WORKDIR /workdir
COPY . .
COPY ./nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
